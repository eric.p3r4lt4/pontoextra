package Model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Dependente {
    private int id;
    private int idFunc;
    private String nome;
    private String data;

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setIdFunc(int idFunc) {
        this.idFunc = idFunc;
    }
    
    
    public void insert(){
        PreparedStatement ps;
        Conexao c = new Conexao();
        
        try{
            ps = c.getConexao().prepareStatement("insert into interesse values(dep_seq.nextval,?,?,?)");
            
            ps.setInt(1, this.idFunc);
            ps.setString(2, this.nome);
            ps.setString(3, this.data);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
}
