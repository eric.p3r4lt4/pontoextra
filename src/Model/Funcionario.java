/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Funcionario {
    private int id;
    private String nome;
    private String email;
    private boolean ativo;
    private String senha;
    
    //gets e sets
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    //CRUD
    public int insert(){
        PreparedStatement ps;
        Conexao c = new Conexao();
        String columnNames[] = {"iduser"}; 
        int coluna = 0;
        
        try{
            ps = c.getConexao().prepareStatement("insert into usuar values(usuario_seq.nextval,?,?,?)", columnNames);
            
            ps.setString(1, this.nome);
            ps.setString(2, this.email);
            ps.setString(3, this.senha);
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next()){
                coluna = rs.getInt(1);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return coluna;
    }
    
    public void update(){
        PreparedStatement ps;
        Conexao c = new Conexao();
                
        try{
            ps = c.getConexao().prepareStatement("update funcionario set nome = ?, email = ?, senha = ? where id = ?");
            
            ps.setInt(4, this.id);
            
            ps.setString(1, this.nome);
            ps.setString(2, this.email);
            ps.setString(3, this.senha);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public ArrayList<Funcionario> getAll(){
        ArrayList<Funcionario> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Statement st;
        Funcionario f;
        
        try{
            st = c.getConexao().createStatement();
            
            ResultSet rs = st.executeQuery("select * from funcionario");
            
            while(rs.next()){
                f = new Funcionario();
                f.setAtivo(rs.getBoolean("ativo"));
                f.setEmail(rs.getString("email"));
                f.setId(rs.getInt("idFunc"));
                f.setSenha(rs.getString("senha"));
                f.setNome(rs.getString("nome"));
                
                lista.add(f);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }
    
    public void delete(){
        PreparedStatement ps;
        Conexao c = new Conexao();
        
        try{
            ps = c.getConexao().prepareStatement("delete from funcionario where id = ?");
            
            ps.setInt(1, this.id);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
}
