/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.*;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class TelosaController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField nomeFunc;
    @FXML
    private TextField matFunc;
    @FXML
    private TextField emailFunc;
    @FXML
    private TextField senhaFunc;
    @FXML
    private TextField reSenhaFunc;
    @FXML
    private TextField nomeDep;
    @FXML
    private TextField dataDep;
    @FXML
    private TableView<Funcionario> tabelaFunc;
    @FXML
    private TableColumn<?, ?> nomeTabFunc;
    @FXML
    private TableColumn<?, ?> matTabFunc;
    @FXML
    private TableColumn<?, ?> emailTabFunc;
    @FXML
    private TableColumn<?, ?> DepTabFunc;
    @FXML
    private TableView<?> tabelaDep;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       this.createColumns();
       this.createLines();
    }    

    @FXML
    private void editarFunc(ActionEvent event) {
    }

    @FXML
    private void deletarFunc(ActionEvent event) {
    }

    @FXML
    private void cadastrarFunc(ActionEvent event) {
    }

    @FXML
    private void cadastrarDep(ActionEvent event) {
    }

    private void createColumns() {
        
    }

    private void createLines() {
        
    }
    
}
